/*Robotics Course
 Valeria Ramirez 
 Assignment: LED Chaser
 Function: LEDs light up in a sequence going from one end to the other.
 The purpose is to demonstrate use of assembly in circuit of LED and knowledge on how to program them to turn on and off
 through the push of a button, in a loop.
 Date: Nov 22, 2018 
 Version 2, (LEDs move faster than in version 1)
 */